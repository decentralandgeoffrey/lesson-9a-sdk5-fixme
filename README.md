This is an older Decentraland scene created in SDK 5.
It is used as a starting point for updateing and fixing a scene, used in Lesson 9 of the Building in Decentraland course of the VR Academy of the Decentraland University.
Course materials are here:
https://docs.google.com/document/d/1AF9l3lYzQsiwFcIexGKP3TI-FFUcMVE34Su6U7FuhZo
