/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'
import { GroundCover} from './modules/groundCover';

const groundMaterial = new Material()
groundMaterial.albedoTexture = new Texture("materials/grass/grass.jpg")
let ground = new GroundCover(0,0, 16, 0.01, 16, groundMaterial, true)


const box = spawnBoxX(2,0.5,8, 0,180,0,  1,1,1)
const myMaterial = new Material()
myMaterial.albedoColor = Color3.FromHexString("#006400")
myMaterial.metallic = 1
myMaterial.roughness = 0
box.addComponent(myMaterial)


const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 2,0,7, 0,180,0, 0.5,0.5,0.5)

const naturePack = spawnGltfX(new GLTFShape("models/NaturePack/tree.gltf"), 8,0,8, 0,180,0,  0.3,0.3,0.3)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 5,0,11, 0,180,0,  .15,.15,.15)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5,1,2, 0,180,0, 0.05, 0.05, 0.05)



let animator = new Animator()
bird.addComponent(animator)
const clipFly = new AnimationState("fly")
animator.addClip(clipFly)
clipFly.play()